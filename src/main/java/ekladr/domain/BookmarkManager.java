package ekladr.domain;

import java.net.URL;
import java.util.*;
import java.util.stream.Collectors;

public class BookmarkManager {

    private final HashMap<URL, Bookmark> bmCollection;

    public BookmarkManager() {
        this.bmCollection = new HashMap<>();
    }

    public void addBookmark(URL url) {
        addBookmark(new Bookmark(url));
    }

    public void addBookmark(Bookmark bookmark) {
        bmCollection.compute(bookmark.getUrl(), (k, v) -> {
            if (v == null) {
                bmCollection
                        .values()
                        .stream()
                        .filter(b -> isSameDomain(b, bookmark))
                        .forEach(b -> {
                            bookmark.getReferences().add((b));
                            b.getReferences().add(bookmark);
                        });
                return bookmark;
            } else
                return v.increaseRating();
        });
    }

    public Collection<Bookmark> getAllBookmarks() {
        return bmCollection.values();
    }

    public void removeBookmark(URL url) {
        bmCollection.remove(url);
    }

    public Collection<Bookmark> getSecureBookmarks() {
        return bmCollection.values().stream().filter(Bookmark::isSecure).collect(Collectors.toSet());
    }

    public boolean isSameDomain(Bookmark bookmark1, Bookmark bookmark2) {
        return bookmark1.getDomain().equals(bookmark2.getDomain());
    }

    public List<Bookmark> getBookmarksByTag(String tag) {
        return getBookmarksByTags(Collections.singletonList(tag));
    }

    public List<Bookmark> getBookmarksByTags(List<String> tags) {
        return bmCollection.values().stream().filter(bookmark -> bookmark.getTags().containsAll(tags)).collect(Collectors.toList());
    }

    public void removeTag(URL url, String tag) {
        bmCollection.computeIfPresent(url, (k, v) -> v.removeTag(tag));
    }

    public Bookmark getBookmark(URL bmUrl) throws IllegalArgumentException {
        Bookmark bm = bmCollection.get(bmUrl);

        if (bm == null) throw new IllegalArgumentException("Element Not Found: " + bmUrl);

        return bm;
    }

    public List<Bookmark> getBookmarksSortedByRating() {
        ArrayList<Bookmark> bms = new ArrayList<>(bmCollection.values());
        bms.sort(Comparator.comparingInt(Bookmark::getRating).reversed());
        return bms;
    }

    public List<Bookmark> getBookmarksSortedByDate() {
        ArrayList<Bookmark> bms = new ArrayList<>(bmCollection.values());
        bms.sort(Comparator.comparing(Bookmark::getTimestamp).reversed());
        return bms;
    }
}