package ekladr.domain;

import java.util.HashMap;

public class User {

    public static HashMap<Integer, User> users = new HashMap<>();
    private static int counter = 0;

    private final int id;
    private final BookmarkManager bookmarkManager;

    public User() {
        id = counter++;
        bookmarkManager = new BookmarkManager();
    }

    int getId() {
        return id;
    }

    BookmarkManager getBookmarkManager() {
        return bookmarkManager;
    }

    public static boolean addUser(User user) {
        users.put(user.id, user);
        return true;
    }
}
