package ekladr.domain;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;
import java.util.stream.Collectors;

public class Bookmark {

    private final URL url;
    private final Set<String> tags;
    private final int rating;
    private final Date timestamp;
    private final List<Bookmark> referenceList;

    public Bookmark(URL url, Set<String> tags) {
        this(url, tags, 1, new Date(), new ArrayList<>());
    }
    Bookmark(URL url, Set<String> tags, int rating, Date timestamp, List<Bookmark> referenceList) {
        this.url = url;
        this.tags = tags;
        this.rating = rating;
        this.timestamp = timestamp;
        this.referenceList = referenceList;
    }

    public Bookmark(URL url) {
        this(url, Collections.emptySet());
    }

    public Bookmark(String urlString) throws MalformedURLException {
        this(new URL(urlString));
    }

    public URL getUrl() {
        return url;
    }

    public int getRating(){
        return rating;
    }

    public boolean isSecure() {
        return url.getProtocol().equals("https");
    }

    public Bookmark increaseRating() {
        return new Bookmark(url, tags, rating + 1, timestamp, referenceList);
    }

    public Set<String> getTags() {
        return tags;
    }

    public boolean hasTag(String tag) {
        return tags.contains(tag);
    }

    public Bookmark removeTag(String tag) {
        return new Bookmark(url, tags.stream().filter(t -> !t.equals(tag)).collect(Collectors.toSet()), rating, timestamp, referenceList);
    }

    public String getDomain() {
        String domain = url.getHost();
        int index = domain.lastIndexOf(".", domain.lastIndexOf(".") - 1);
        return domain.substring(index + 1);
    }

    public boolean addReference(Bookmark bookmark) {
        if (referenceList.contains(bookmark)) {
            return false;
        }
        referenceList.add(bookmark);
        return true;
    }

    public List<Bookmark> getReferences() {
        return referenceList;
    }

    public Date getTimestamp() {
        return timestamp;
    }
}
