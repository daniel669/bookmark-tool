package ekladr.domain;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.Mock;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class BookmarkTest {

    @Mock
    Bookmark bookmark;
    static URL urlMock;

    @BeforeAll
    static void init(){
        urlMock = mock(URL.class);
    }

    @BeforeEach
    void setUp(){
        bookmark = new Bookmark(urlMock);
    }

    @Test
    void testMalformedUrlThrows() {
        assertThrows(MalformedURLException.class, () -> new Bookmark("httpp://www.google.at"));
    }

    @Test
    void testMalformedUrlWithTagThrows() {
        assertThrows(MalformedURLException.class, () -> new Bookmark(new URL("httpp://www.google.at"), Collections.singleton("test")));
    }

    @Test
    void testGetUrl(){
        assertEquals(urlMock, bookmark.getUrl());
    }

    @Test
    void testBookmarkFromStringWithoutTag() {
        try{
            Bookmark bm = new Bookmark("https://www.google.at");
            assertEquals("https://www.google.at", bm.getUrl().toString());
            assertTrue(bm.getTags().isEmpty());
        } catch(Exception ex){
            fail();
        }
    }

    @Test
    void testBookmarkWithTag() {
        try{
            Bookmark bm = new Bookmark(new URL("https://www.google.at"), Collections.singleton("test"));
            assertEquals("https://www.google.at", bm.getUrl().toString());
            assertTrue(bm.hasTag("test"));
        } catch(Exception ex){
            fail();
        }
    }

    @Test
    void testBookmarkWithoutTag() {
        assertTrue(bookmark.getTags().isEmpty());
    }

    @Test
    void testBookmarkSecure() {
        when(urlMock.getProtocol()).thenReturn("https");
        assertTrue(bookmark.isSecure());
    }

    @Test
    void testBookmarkNotSecure() {
        when(urlMock.getProtocol()).thenReturn("http");
        assertFalse(new Bookmark(urlMock).isSecure());
    }

    @Test
    void testGetRating() {
        Bookmark bookmark = new Bookmark(urlMock, Collections.emptySet());
        assertEquals(1, bookmark.getRating());
    }

    @ParameterizedTest
    @ValueSource(ints = {1, 2, 3, 4, 5})
    void testIncreaseRating(int amountOfTimes) {
        Bookmark bookmark = new Bookmark(urlMock, Collections.emptySet());
        for (int i = 1; i < amountOfTimes; i++) {
            bookmark = bookmark.increaseRating();
        }
        assertEquals(amountOfTimes, bookmark.getRating());
    }

    @ParameterizedTest
    @CsvSource({
            "https://google.com, google.com",
            "http://www.google.com, google.com",
            "https://www.google.com, google.com",
            "https://developers.google.com, google.com",
            "https://java.developers.google.com?test=test, google.com",
            "https://google.com?test=test, google.com",
            "https://google.com/index.html?test=test, google.com",
            "https://google.com/pub?test=test, google.com",
            "https://google.com/pub/index.html?test=test, google.com"
    })
    void testGetDomain(String url, String domain) {
        try {
            assertEquals(domain, new Bookmark(url).getDomain());
        } catch (Exception ex) {
            fail(ex.getMessage());
        }
    }

    @Test
    void testAddReference() {
        Bookmark bookmarkMock = mock(Bookmark.class);
        assertTrue(bookmark.addReference(bookmarkMock));
    }

    @Test
    void testAddReferenceTwice() {
        Bookmark bookmarkMock = mock(Bookmark.class);
        bookmark.addReference(bookmarkMock);
        assertFalse(bookmark.addReference(bookmarkMock));
    }
}
