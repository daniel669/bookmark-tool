package ekladr.domain;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.*;

public class UserTest {
    @Mock
    User user;

    @BeforeEach
    void setUp() {
        user = new User();
        User.users = new HashMap<>();
    }

    @Test
    void testUser() {
        assertNotNull(user);
    }

    @Test
    void testAddUser() {
        assertTrue(User.addUser(user));
    }

    @Test
    void testCountUsers() {
        User.addUser(new User());
        User.addUser(new User());
        assertEquals(2, User.users.size());
    }

    @Test
    void testHasBookmarkManager() {
        assertNotNull(user.getBookmarkManager());
    }
}
